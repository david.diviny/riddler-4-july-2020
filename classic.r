generate_centered_pentagon <- function(n) {(5*n^2 + 5*n + 2)/2}

test_square <- function(n) {sqrt((n-1)/2) %% 1 == 0}

sequence <- generate_centered_pentagon(5:100)

answer <- sequence[test_square(sequence)] - 1
